=== 基础知识 ===

# `valueOf`使用常量，未创建新的对象
# `new`关键字会创建新对象
# 自动装箱使用`valueOf`方法

=== 常用方法 ===

# `parseBoolean(string)`转换`string`为`boolean`值
# `valueOf(string)`转换`string`为`Boolean`类型，使用常量
